﻿#include <iostream>
#include <chrono>
using namespace std;

class Timer
{
private:
	using clock_t = std::chrono::high_resolution_clock;
	using second_t = std::chrono::duration<double, std::ratio<1> >;
	std::chrono::time_point<clock_t> m_beg;
public:
	Timer() : m_beg(clock_t::now())
	{
	}
	void reset()
	{
		m_beg = clock_t::now();
	}
	double elapsed() const
	{
		return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
	}
};

void qsort(int a, int b, int* x)
{
	if (a >= b)
		return;
	int m, k,l,r; 
	m = ((a + b)/2)+1;
	k = x[m];
	l = a - 1;
	r = b + 1;
	while (1)
	{
		do
		{
			l++;
		} while (x[l] < k);

		do 
		{
			r--;
		} while (x[r] > k);

		if (l >= r)
		{
			break;
		}

		swap(x[l], x[r]);
	}

	r = l;
	l = l - 1;
	qsort(a, l, x);
	qsort(r, b, x);

}

void bsort(int* mas, int n)
{
	for (int i = 1; i < n; i++)
	{
		if (mas[i] >= mas[i - 1])
			continue;
		int j = i - 1;
		while (j >= 0 && mas[j] > mas[j + 1])
		{
			std::swap(mas[j], mas[j + 1]);
			j--;
		}
	}
}


int main()
{
	srand(time(0));

	int const N = 10;
	int mas1[N];
	int mas2[N];

	for (int i = 0; i < N; i++)
		mas1[i] = rand();

	Timer a;
	qsort(0, N-1, mas1);
	cout << "Time" << a.elapsed() << endl;

	//for (int i = 0;i < N;i++)
	//{
	//	cout << mas1[i] <<" ";
	//}

	for (int i = 0; i < N;i++)
		mas2[i] = rand();

	/*Timer b;
	bsort(mas2, N);
	cout << "Time " << b.elapsed() << endl;*/

	for (int i = 0;i < N;i++)
	{
		cout << mas2[i] << " ";
	}


	return 0;


	// qsort:
	// n = 10  => Time  = 2.4*10^(-6)
	// n = 100 => Time  = 2.8*10^(-5)
	// n = 1000 => Time  = 0.0003142
	// n = 10000 => Time  = 0.0062

	// bsort
}	// n = 10  => Time  = 3*10^(-6)
	// n = 100 => Time  = 0.000295
	// n = 1000 => Time  = 0.017
	// n = 10000 => Time  = 1.825s