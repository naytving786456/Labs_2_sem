﻿#include <iostream>
#include <chrono>
using namespace std;

struct T_list
{
    T_list* next; 

    int num; 


};

class Timer
{
private:
    using clock_t = std::chrono::high_resolution_clock;
    using second_t = std::chrono::duration<double, std::ratio<1> >;
    std::chrono::time_point<clock_t> m_beg;
public:
    Timer() : m_beg(clock_t::now())
    {
    }
    void reset()
    {
        m_beg = clock_t::now();
    }
    double elapsed() const
    {
        return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
    }
};


void add(T_list* head, int num)
{
    T_list* p = new T_list;
    p->num = num;

    p->next = head->next;
    head->next = p;
}


void print(T_list* head)
{
    T_list* p = head->next;
    while (p != nullptr)
    {
        cout << p->num << endl;
        p = p->next;
    }
}

bool search(T_list* head, int k)
{
    T_list* p = head->next;
    while (p->next->next != nullptr)
    {
        if (p->num == k)
        {
            return true;
            
        }

        else
        {
            p = p->next;
        }
    }
    return false;
}

int search_m(int* mas, int el, int N)
{   
    for (int i = 0;i < N;i++)
    {
        if (mas[i] == el)
            return i;
    }

    return -1;

}

void clear(T_list* head)
{
    T_list* tmp;
    T_list* p = head->next;

    while (p != nullptr)
    {
        tmp = p;
        p = p->next;
        delete tmp;
    }
}

int main()
{

    T_list* head = new T_list;
    head->next = nullptr;

    int const N = 10000;
    int M = 100000;

    int mas[N];

    Timer a;
    // массив
    for (int i = 0;i < N;i++)
    {
        mas[i] += i;
    }

    for (int k = 0; k < M;k++)
    {
        if (search_m(mas, k, N))
        {
           // cout << "Find" << endl;
        }
        else
        {
           // cout << "Nothing" << endl;
        }
    }
    cout << "Time" << a.elapsed() << endl;

    //список
    Timer b;

    for (int i = 0; i < N;i++)
    {
        add(head, i);
    }
    
    for (int k = 0;k < M;k++)
    {
        if (search(head, k)) {

            //cout << "Find" << endl;

        }
        else
        {
            //cout << "Nothing" << endl;
        }
            
    }
    cout << "Time" << b.elapsed() << endl;

    clear(head);
    
    delete head;
}

// Список - при M=1000(кол-во эл которые надо найти),N=10000(кол-во эл в массиве), T = 0.018237
//Массив - при M=1000(кол-во эл которые надо найти),N=10000(кол-во эл в массиве) , T = 0.057631
//M=10000=> T = 0.192135(List)
//M=10000 => T = 0.22058(Mass)