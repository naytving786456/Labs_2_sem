﻿#include <iostream>
#include <fstream>

using namespace std;


struct Elem
{
    int data;// данные
    //указатели на соседние вершины
    Elem* left;
    Elem* right;
    Elem* parent;
};

//инициализация вершины
Elem* MAKE(int data, Elem* p)
{
    Elem* q = new Elem;// выделяем память под вершину
    q->data = data;
    q->left = nullptr;
    q->right = nullptr;
    q->parent = p;
    return q;
}

//Добавление элемента
void ADD(int data, Elem*& root)
{
    if (root == nullptr)
    {
        root = MAKE(data, nullptr);
        return;
    }
    Elem* v = root;
    while ((data < v->data && v->left != nullptr) || (data > v->data && v->right != nullptr))
        if (data < v->data)
            v = v->left;
        else
            v = v->right;
    if (data == v->data)
        return; 
    Elem* u = MAKE(data, v);
    if (data < v->data)
        v->left = u;
    else
        v->right = u;
}

//обход дерева
void PASS(Elem* v)
{
    if (v == nullptr)
        return;
    PASS(v->left);
    //инфиксный
    cout << v->data << endl;
    PASS(v->right);

}

Elem* SEARCH(int data, Elem* v)//v-элемент от которого начинаем поиск
{
    if (v == nullptr)
        return v;
    if (v->data == data)
        return v;
    if (data < v->data)
        return SEARCH(data, v->left);
    else
        return SEARCH(data, v->right);
}


void DELETE(int data, Elem*& root)
{
    // проверка на существование такого элемента
    Elem* u = SEARCH(data, root);
    if (u == nullptr)
        return; 

    // Удаление элемента(особый случай)
    if (u->left == nullptr && u->right == nullptr && u == root)
    {
        delete root;
        root = nullptr;
        return;
    }

    //Если оба потомка присутсвуют 
    if (u->left == nullptr && u->right != nullptr && u == root)
    {
        Elem* t = u->right; 
        while (t->left != nullptr)
            t = t->left;
        u->data = t->data;
        u = t;
    }

    if (u->left != nullptr && u->right == nullptr && u == root)
    {
        Elem* t = u->left;
        while (t->right != nullptr)
            t = t->right;
        u->data = t->data;
        u = t;
    }

    if (u->left != nullptr && u->right != nullptr)
    {
        Elem* t = u->right;
        while (t->left != nullptr)
            t = t->left;
        u->data = t->data;
        u = t;
    }

    Elem* t;
    if (u->left == nullptr)
        t = u->right;
    else
        t = u->left;
    if (u->parent->left == u)
        u->parent->left = t;
    else
        u->parent->right = t;
    if (t != nullptr)
        t->parent = u->parent;
    
    delete u;

}

Elem* Search(int data, Elem* v, int num)
{
    ofstream out("output.txt");

    if (v == nullptr)
    {
        cout << data << "-n"<<endl;
        out << data << "-n" <<" \n";
        return v;
    }

    else if (v->data == data)
    {
        cout <<v-> data << "in depth -" <<num<<endl;
        out << v->data << "in depth - " <<num<<"\n";
        return v;
    }

    else if (data < v->data)
    {
       return Search(data, v->left, num+1);
    }

    else
    {
      return Search(data, v->right, num+1);
    }

}

void CLEAR(Elem*& v)
{
    if (v == nullptr)
        return;

    CLEAR(v->left);
    CLEAR(v->right);

    delete v;
    v = nullptr;
}

int main()
{
    Elem* root = nullptr;
    ifstream in("input.txt");
    if (!in.is_open())
        cout << "ERROR!!!! PROBLEM WITH FILE" << endl;

    else
    {
        cout << "FILE IS OPEN!!!" << endl;

        char a;
        int b;

        while (!in.eof())
        {
            in >> a;
            in >> b;

            if (a == '+')
            {
                ADD(b, root);
                cout << "+" << b << endl;
            }

            else if (a == '-')
            {
                DELETE(b, root);
                cout << "-" << b << endl;
            }

            else if (a == '?')
            {
                Search(b, root,1);
            }

            else if (a == 'E')
                break;

        }



    }

    
    CLEAR(root);
    in.close();
    return 0;


}