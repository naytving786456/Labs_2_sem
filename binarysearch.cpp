﻿#include <iostream>
#include <chrono>

using namespace std;


class Timer
{
private:
	using clock_t = std::chrono::high_resolution_clock;
	using second_t = std::chrono::duration<double, std::ratio<1> >;
	std::chrono::time_point<clock_t> m_beg;
public:
	Timer() : m_beg(clock_t::now())
	{
	}
	void reset()
	{
		m_beg = clock_t::now();
	}
	double elapsed() const
	{
		return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
	}
};

int binary_search(int start, int end, int el,int* mas) 
{
	if (el < mas[start])
		return -1;
	if (el == mas[start])
		return 1;
	if (el >= mas[end])
		return -1;

	int left = start;
	int right = end ;

	while ((left + 1) < right)
	{
		int c = (left + right) / 2;
		if (el > mas[c])
			left = c;
		else
			right = c;
	}

	if (el==mas[right])
	{
		return right;
	}
	else
	{
		return 0;
	}
}

void qsort(int a, int b, int* x)
{
	if (a >= b)
		return;
	int m, k, l, r;
	m = ((a + b) / 2) + 1;
	k = x[m];
	l = a - 1;
	r = b + 1;
	while (1)
	{
		do
		{
			l++;
		} while (x[l] < k);

		do
		{
			r--;
		} while (x[r] > k);

		if (l >= r)
		{
			break;
		}

		swap(x[l], x[r]);
	}

	r = l;
	l = l - 1;
	qsort(a, l, x);
	qsort(r, b, x);

}

double Function(double x)
{
	return log(x) - 1;
}

double Bi_section(double left , double right)
{
	double eps = 1e-4;
	while (1)
	{
		double x = (left + right) / 2;
		if ((abs(x - left) < eps) && (abs(x - right) < eps))
			return x;
		else if (Function(x) * Function(left) > 0)
			left = x;
		else
			right = x;
	}

}

int main()
{
	int const N = 10;
	int const M = 10;

	int start = 0;
	int end = N - 1;

	int left = 0;
	int right = 3;

	int mas_1[M] ;
	int mas_2[N];
	for (int i = 0; i < M;i++)
	{
		mas_1[i] = rand();
	}
	for (int i = 0; i < N; i++)
	{
		mas_2[i] = mas_1[i];
	}

	qsort(0, M - 1, mas_1);
	
	cout << "First massive"<<endl;
	for (int i = 0;i < M;i++)
	{
		
		cout << mas_1[i] << endl;

	}

	cout << "Second massive"<<endl;
	for (int i = 0; i < N;i++)
	{
		
		cout << mas_2[i] << endl;
	}

	Timer a;

	//for (int i = 0;i < N;i++)
	//{
	//	cout << binary_search(start, end, mas_2[i], mas_1) << endl;
	//}

	//for (int i = 0; i < M;i++)
	//{
	//	for (int j = 0;j < N;j++)
	//	{
	//		if (mas_2[j] == mas_1[i])
	//		{
	//			cout << "Find!!!";
	//			break;
	//		}
	//	}
	//}

	//cout << "Time" << a.elapsed() << endl;

	cout << "x= " << Bi_section(left, right) << endl;

	return 0;
}